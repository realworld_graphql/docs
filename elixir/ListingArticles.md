# Listing Articles

Phoenix comes with a handfull of mix tasks, allowing us to create a new
Context, Ecto Migration and so an as a starting point. To create something
we can use for the articles run the following command:
```bash
mix phx.gen.context Content Article articles \
> slug:string \
> title:string \
> description:string \
> body:string
```
This creates a context file housing all the logic, a file containing an article
struct, a test file and the database migration.

This gives as a good starting ground and to get the article query up and running
we can use the list_articles function the generator created for us.

First we need to create GraphQL type for the article and put it in the root query.
```elixir
defmodule RealWorldWeb.Schema do
  use Absinthe.Schema
  alias RealWorldWeb.Resolvers

  object :article do
    field :slug, non_null(:string)
    field :title, :string
    field :description, :string
    field :body, :string
  end

  query do
    field :articles, list_of(:article) do
      resolve(&Resolvers.Content.list_articles/3)
    end
  end
end
```
Now lets create a new file for our resolver functions handling content. And create 
our first resolver
```elixir
defmodule RealWorldWeb.Resolvers.Content do
  alias RealWorld.Content

  def list_articles(_, _, _) do
    {:ok, Content.list_articles()}
  end
end
```
Lastly, lets create a test for this query.
```elixir
defmodule RealWorld.Schema.Query.ArticleTest do
  use RealWorldWeb.ConnCase, async: true

  @query """
  query articles {
    articles {
      slug
    }
  }
  """
  test "menuItems field returns menu items" do
    Enum.each(1..2, fn _ -> Factory.create_article() end)

    conn = build_conn()
    conn = get conn, "/api", query: @query

    assert json_response(conn, 200) == %{
             "data" => %{
               "articles" => [
                 %{"slug" => "slug_1"},
                 %{"slug" => "slug_2"}
               ]
             }
           }
  end
end
```

The Factory just creates some articles and puts them in the database.
```elixir
defmodule Factory do
  def create_article() do
    int = :erlang.unique_integer([:positive, :monotonic])

    params = %{
      slug: "slug_#{int}",
      title: "Some Title#{int}",
      description: "Descibes the article",
      body: "The body of the article"
    }

    %RealWorld.Content.Article{}
    |> RealWorld.Content.Article.changeset(params)
    |> RealWorld.Repo.insert!()
  end
end
```








