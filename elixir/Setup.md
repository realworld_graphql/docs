# Project Setup

The Phoenix Framework comes with a nice generator tool which helps
setup the project. The default project comes with support for server
renderd html and webpack for asset bundling. Because all we want is
a GraphQL API we tell the generator that we don't want those in our
project.

To create the project run the following command:
`mix phx.new --database postgres --no-webpack --no-html real_world`

Afterwards we need to add some dependencies:

```elixir
def deps do
  [
    # ...
    {:absinthe, "~> 1.4"}, # Absinthe itself
    {:absinthe_plug, "~> 1.4"}, # AbsinthePlug for Phoenix integration
  ]
end
```

To integrate Absinthe into Phoenix we just need to create a schema and 
configure the routes:

```elixir
defmodule RealWorldWeb.Schema do
  use Absinthe.Schema

  query do
    field :test, :string do
      resolve fn _, _, _ ->
        {:ok, "test"}
      end
    end
  end
end

defmodule RealWorldWeb.Router do
  #...
  scope "/" do
    pipe_through :api

    forward "/api", Absinthe.Plug, schema: RealWorldWeb.Schema

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: RealWorldWeb.Schema,
      interface: :simple
  end
end
```

That's it, we now have a GraphQL API.

