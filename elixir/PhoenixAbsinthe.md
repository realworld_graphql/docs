# Elixir

I decided to use [Elixir](https://elixir-lang.org/) to implement my first 
example of the RealWorld API, because I'm fairly comfortable using Elixir 
and [Phoenix](https://www.phoenixframework.org/) and there is a great
[book](https://pragprog.com/book/wwgraphql/craft-graphql-apis-in-elixir-with-absinthe) 
about using [Absinthe](https://absinthe-graphql.org/).
