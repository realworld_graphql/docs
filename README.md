![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

# RealWorld GraphQL

This project is an attemp in creating the [RealWorld App](https://github.com/gothinkster/realworld)
using GraphQL.

## Reasons for creating this project

I had to find a topic for my bachelor thesis. I work at a small company and 
we use GraphQL for all our new projects. Using GraphQL for a while I really 
started to enjoy using it. I also enjoy working with functional languages, but
at work we use mostly python and javascript. So I thought I could implement a
GraphQL API using a functional language and see how I like doing it. In order
to have bit more to right about in my bachelor thesis I decided to implement 
the same API in two different languages and comparing them on couple of aspects,
like developer expirience, documentation, testing, etc.

Having the basic Idea in my head, I searched for a project that would have a 
reasonable scope while still covering most of what GraphQL can do. I looked at
the [RealWorld App](https://github.com/gothinkster/realworld) in the past and
really liked the idea behind it. I saw on their page, that they were planning 
on adding some GraphQL implementation in the future and thought I could take a
stab at implementing an example which I could also use in my bachelor thesis.
