# Creating a Schema

Going through the [spec](https://github.com/gothinkster/realworld/tree/master/spec)
for the RealWorld App, I first tried to identify all the actions a potantial
user could make. I came up with the following list.

* Register as new user
* Login as existing user
* List tags
* Create a new article
* Edit an existing article
* List articles
* Search articles
* Read article
* Comment article
* Delete a comment
* Read author profile
* Follow author
* Unfollow author

I then came up with the following Schema as a starting point. 

## Types
```
type User {
    email: String!
    token: String!
    username: String!
}

type Profile {
    username: String!
    bio: String
    image: String
    viewerIsFollowing: Bool
}

type Article {
    slug: String!
    title: String!
    description: String!
    body: String!
    tagList: [String]!
    createdAt: Date!
    updatedAt: Date!
    viewerHasFavorited: Bool!
    favoritesCount: Number!
    author: Profile!
    comments: [Comment]
}

type ArticleFilter {
    tag: String
    author: String

    limit: Number
    offset: Number
}

type Comment {
    createdAt: Date!
    updatedAt: Date!
    body: String!
    author: Profile!
}

type LoginInput {
    email: String!
    password: String!
}

type RegisterInput {
    username: String!
    email: String! 
    password: String!
}

type UpdateUserInput {
    id: ID
    emai: String
    bio: String
    image: String
}

type CreateArticleInput {
    title: String!
    description: String!
    body: String!
    tagList: [String]!
}

type UpdateArticleInput {
    id: ID!
    title: String
    description: String
    body: String
}

type CreateCommentInput {
    articleSlug: String!
    comment: {
        body: String!
    }!
}

type Query {
    me: {
        user: User
        feed: [Article]
        favorites: [Article]
    }
    profile(username: String): Profile
    articles(filter: ArticleFilter): [
        Article
    ]
    article(slug: String): Article
    tags: [String]
}

type Mutation {
    user {
        login(input: LoginInput!)
        register(input: RegisterInput!)
        updateUser(input: UpdateUserInput)
        followUser(username: String!)
        unfollowUser(username: String!)
    }
    content {
        createArticle(input: CreateArticleInput!)
        updateArticle(input: UpdateArticleInput!)
        deleteArticle(id: ID)
        addComment(input: CreateCommentInput)
        deleteComment(id: ID)
        favoriteArticle(slug: String!)
        unfavoriteArticle(slug: String!)
    }
}
```

Data that is different for each user is grouped into the `me` object. Fields on 
public data like the articles favorited field have a viewer prefix (copied from 
the github graphql api)
